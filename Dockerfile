FROM openjdk:8-jdk-slim AS builder
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ARG BASEURL=https://download.appdynamics.com/download/prox/download-file
ARG VERSION
ARG USER
ARG PASSWORD

ADD start.sh /opt/appdynamics/synthetics/

RUN apt-get update
RUN apt-get install --fix-missing -q -y curl unzip
RUN echo "ulimit -n 65535" >> /etc/profile
RUN echo "ulimit -u 8192" >> /etc/profile
RUN echo "vm.swappiness = 10" >> /etc/sysctl.conf
RUN curl --referer http://www.appdynamics.com -c /tmp/cookies.txt -d "username=${USER}&password=${PASSWORD}" https://login.appdynamics.com/sso/login/
RUN curl -L -o /tmp/appdynamics-synthetic-server.zip -b /tmp/cookies.txt ${BASEURL}/synthetic-server/${VERSION}/appdynamics-synthetic-server-${VERSION}.zip
RUN unzip /tmp/appdynamics-synthetic-server.zip -d /opt/appdynamics/synthetics

FROM openjdk:8-jdk-slim
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ENV DB_HOST="eum" \
    DB_PORT="3388" \
    DB_ROOT_USER="root" \
    DB_ROOT_PWD="appdynamics" \
    DB_USERNAME="eum_user" \
    DB_USER_PWD="appdynamics" \
    COLLECTOR_HOST="eum" \
    COLLECTOR_PORT="7001" \
    KEY_STORE_PASSWORD="appdynamics"

COPY --from=builder /opt/appdynamics /opt/appdynamics
COPY --from=builder /etc/profile /etc/profile
COPY --from=builder /etc/sysctl.conf /etc/sysctl.conf

RUN apt-get update \
    && apt-get install --fix-missing -q -y libaio1 libnuma1 wget python python-pip

EXPOSE 10101 10102 12101 12102

CMD [ "/bin/bash", "-c", "/opt/appdynamics/synthetics/start.sh" ]