#!/bin/bash

APPD_ROOT="/opt/appdynamics"
APPD_SYNTH="$APPD_ROOT/synthetics"

cd $APPD_SYNTH

if [ ! -f "inputs.groovy" ]; then
    cp inputs.groovy.sample inputs.groovy
    sed -i -e "/db_host/c\db_host = \"$DB_HOST\"" inputs.groovy
    sed -i -e "/db_port/c\db_port = \"$DB_PORT\"" inputs.groovy
    sed -i -e "/db_root_user/c\db_root_user = \"$DB_ROOT_USER\"" inputs.groovy
    sed -i -e "/db_root_pwd/c\db_root_pwd = \"$DB_ROOT_PWD\"" inputs.groovy
    sed -i -e "/db_username/c\db_username = \"$DB_USERNAME\"" inputs.groovy
    sed -i -e "/db_user_pwd/c\db_user_pwd = \"$DB_USER_PWD\"" inputs.groovy
    sed -i -e "/collector_host/c\collector_host = \"$COLLECTOR_HOST\"" inputs.groovy
    sed -i -e "/collector_port/c\collector_port = \"$COLLECTOR_PORT\"" inputs.groovy
    sed -i -e "/key_store_password/c\key_store_password = \"$KEY_STORE_PASSWORD\"" inputs.groovy
    unix/deploy.sh install
else
    unix/deploy.sh start
fi

tail -f /dev/null